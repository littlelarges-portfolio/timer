import 'dart:async';

class AppEvents {
  static StreamController<void> onTick = StreamController<void>();
  static StreamController<void> onTimerEnds = StreamController<void>();
  static StreamController<void> onTimerStops = StreamController<void>();
}
