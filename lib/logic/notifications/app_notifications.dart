import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class AppNotifications {
  late final BuildContext context;
  bool isSnackbarVisible = false;

  static void init(
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin) {
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    const DarwinInitializationSettings initializationSettingsDarwin =
        DarwinInitializationSettings();
    const InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsDarwin,
    );
    flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  void initContext(BuildContext context) {
    this.context = context;
  }

  static void showNotification(
    FlutterLocalNotificationsPlugin fln,
    int id,
    String title,
    String body,
  ) async {
    NotificationDetails notificationDetails = const NotificationDetails(
        android: AndroidNotificationDetails('channelId', 'channelName',
            importance: Importance.high),
        iOS: DarwinNotificationDetails());

    await fln.show(id, title, body, notificationDetails);
  }

  void showSnackBar({required String content}) {
    if (!isSnackbarVisible) {
      Duration snackBarDuration = const Duration(seconds: 2);

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(content),
          duration: snackBarDuration,
          backgroundColor: Colors.red,
        ),
      );

      isSnackbarVisible = true;

      Future.delayed(snackBarDuration, () {
        isSnackbarVisible = false;
      });
    }
  }
}
