part of 'timer_bloc.dart';

sealed class TimerState {
  late Duration time = const Duration();
  late Timer mainTimer = Timer(const Duration(), () {});

  void resetTimer() {
    mainTimer.cancel();
  }
}

final class TimerInitial extends TimerState {
  TimerInitial({this.initTime = const Duration()}) {
    resetTimer();

    time = initTime;
  }

  final Duration initTime;
}

final class TimerTick extends TimerState {
  TimerTick({required Duration initTime}) {
    time = initTime;
    mainTimer = Timer.periodic(const Duration(milliseconds: 100), (timer) {
      final int milliseconds = time.inMilliseconds + -100;

      print(time.inSeconds.remainder(60));

      time = Duration(milliseconds: milliseconds);

      AppEvents.onTick.add(0);

      if (time.inMilliseconds == 0) {
        AppEvents.onTimerEnds.add(0);
      }
    });
  }
}

final class TimerPause extends TimerState {
  TimerPause({required this.state}) {
    state.resetTimer();

    time = state.time;
  }

  final TimerState state;
}
