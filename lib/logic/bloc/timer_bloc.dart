import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../../main.dart';
import '../events/app_events.dart';

part 'timer_event.dart';
part 'timer_state.dart';

class TimerBloc extends Bloc<TimerEvent, TimerState> {
  TimerBloc() : super(TimerInitial()) {
    on<OnStart>((event, emit) {
      if (event.initTime.compareTo(const Duration()) != 0) {
        emit(TimerTick(initTime: event.initTime));
      } else {
        appNotifications.showSnackBar(content: 'Choose a time!');
      }
    });
    on<OnReset>((event, emit) {
      emit(TimerInitial(initTime: event.resetTime));
    });
    on<OnPause>((event, emit) {
      emit(TimerPause(state: event.state));
    });
  }
}
