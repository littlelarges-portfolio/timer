part of 'timer_bloc.dart';

@immutable
sealed class TimerEvent {}

class OnTick extends TimerEvent {}

class OnPause extends TimerEvent {
  OnPause({required this.state}) {
    state.resetTimer();
  }

  final TimerState state;
}

class OnStart extends TimerEvent {
  OnStart({this.initTime = const Duration()});

  final Duration initTime;
}

class OnReset extends TimerEvent {
  OnReset({required this.state, required this.resetTime}) {
    state.resetTimer();

    AppEvents.onTimerStops.add(0);
  }

  final TimerState state;
  final Duration resetTime;
}

class OnResume extends TimerEvent {}
