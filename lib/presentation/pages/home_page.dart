import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker_bdaya/flutter_datetime_picker_bdaya.dart';

import '../../logic/bloc/timer_bloc.dart';
import '../../logic/events/app_events.dart';
import '../../logic/notifications/app_notifications.dart';
import '../../main.dart';
import '../components/home_page/time.dart';
import '../components/home_page/timer_controller.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DateTime selectedTime = DateTime(DateTime.now().year);

  @override
  void initState() {
    super.initState();

    AppEvents.onTick.stream.listen((event) {
      setState(() {});
    });

    AppEvents.onTimerEnds.stream.listen((event) {
      timerBloc.add(OnReset(
          state: timerBloc.state,
          resetTime: Duration(
              hours: selectedTime.hour,
              minutes: selectedTime.minute,
              seconds: selectedTime.second,
              milliseconds: selectedTime.millisecond)));

      AppNotifications.showNotification(
          flutterLocalNotificationsPlugin, 0, 'Timer', 'Time is over!');
    });

    AppEvents.onTimerStops.stream.listen((event) {
      setState(() {});
    });

    appNotifications.initContext(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => timerBloc,
      child: Scaffold(
        body: Center(
          child: BlocBuilder<TimerBloc, TimerState>(
            builder: (context, state) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text('Timer',
                      style: TextStyle(
                        fontSize: 20,
                      )),
                  const SizedBox(height: 20),
                  Time(time: state.time),
                  const SizedBox(height: 20),
                  ElevatedButton(
                      onPressed: timerBloc.state is TimerInitial
                          ? () {
                              DatePickerBdaya.showTimePicker(
                                context,
                                showTitleActions: true,
                                onChanged: (date) {
                                  selectedTime = date;

                                  timerBloc.state.time = Duration(
                                      hours: selectedTime.hour,
                                      minutes: selectedTime.minute,
                                      seconds: selectedTime.second,
                                      milliseconds: selectedTime.millisecond);

                                  setState(() {});
                                },
                                onConfirm: (date) {
                                  selectedTime = date;

                                  timerBloc.state.time = Duration(
                                      hours: selectedTime.hour,
                                      minutes: selectedTime.minute,
                                      seconds: selectedTime.second,
                                      milliseconds: selectedTime.millisecond);

                                  setState(() {});
                                },
                                currentTime: selectedTime,
                                locale: LocaleType.en,
                              );
                            }
                          : null,
                      // onPressed: null,
                      style: ElevatedButton.styleFrom(
                        disabledBackgroundColor: Colors.grey,
                      ),
                      child: const Text(
                        'Select time',
                      )),
                  TimerController(
                      state: timerBloc.state, selectedTime: selectedTime),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
