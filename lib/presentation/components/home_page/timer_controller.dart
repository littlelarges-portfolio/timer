import 'package:flutter/material.dart';

import '../../../logic/bloc/timer_bloc.dart';
import '../../../main.dart';

class TimerController extends StatelessWidget {
  const TimerController(
      {super.key, required this.state, required this.selectedTime});

  final TimerState state;
  final DateTime selectedTime;

  @override
  Widget build(BuildContext context) {
    return state is TimerInitial
        ? ElevatedButton(
            onPressed: () {
              timerBloc.add(OnStart(
                  initTime: Duration(
                      hours: selectedTime.hour,
                      minutes: selectedTime.minute,
                      seconds: selectedTime.second,
                      milliseconds: selectedTime.millisecond)));
            },
            child: const Text(
              'Start',
            ))
        : Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              state is TimerTick
                  ? ElevatedButton(
                      onPressed: () {
                        timerBloc.add(OnPause(
                          state: timerBloc.state,
                        ));
                      },
                      child: const Text(
                        'Pause',
                      ))
                  : ElevatedButton(
                      onPressed: () {
                        timerBloc.add(OnStart(initTime: timerBloc.state.time));
                      },
                      child: const Text(
                        'Resume',
                      )),
              const SizedBox(
                width: 10,
              ),
              ElevatedButton(
                  onPressed: () {
                    timerBloc.add(OnReset(
                        state: timerBloc.state,
                        resetTime: Duration(
                            hours: selectedTime.hour,
                            minutes: selectedTime.minute,
                            seconds: selectedTime.second,
                            milliseconds: selectedTime.millisecond)));
                  },
                  child: const Text(
                    'Stop',
                  ))
            ],
          );
  }
}
