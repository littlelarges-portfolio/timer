import 'package:flutter/material.dart';
import 'package:timer/presentation/components/home_page/time/time_card.dart';

import 'time/time_card_mini.dart';

class Time extends StatelessWidget {
  const Time({Key? key, required this.time}) : super(key: key);

  final Duration time;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 290,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              TimeCard(time: time.inHours, isHour: true),
              const SizedBox(width: 10),
              TimeCard(time: time.inMinutes),
              const SizedBox(width: 10),
              TimeCard(time: time.inSeconds),
            ],
          ),
          const SizedBox(width: 10),
          Positioned(right: 0, child: TimeCardMini(time: time.inMilliseconds)),
        ],
      ),
    );
  }
}
