import 'dart:math';

import 'package:flutter/material.dart';

class TimeCard extends StatelessWidget {
  TimeCard({Key? key, required this.time, this.isHour = false})
      : super(key: key) {
    formattedTime =
        time.remainder(isHour ? 24 : 60).abs().toString().padLeft(2, '0');
  }

  final int time;
  final bool isHour;

  late final String formattedTime;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        width: 50,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          // color: Colors.green,
          color: Color.fromRGBO(
              255, Random().nextInt(156), Random().nextInt(156), 1),
        ),
        child: Center(
            child: Text(formattedTime,
                style: const TextStyle(color: Colors.white))));
  }
}