import 'dart:math';

import 'package:flutter/material.dart';

class TimeCardMini extends StatelessWidget {
  TimeCardMini({Key? key, required this.time}) : super(key: key) {
    formattedTime = (time / 10).remainder(100).abs().round().toString().padLeft(2, '0');
  }

  final int time;

  late final String formattedTime;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 25,
        width: 50,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          // color: Colors.green,
          color: Color.fromRGBO(
              Random().nextInt(156), Random().nextInt(156), Random().nextInt(156), 1),
        ),
        child: Center(
            child: Text(formattedTime,
                style: const TextStyle(color: Colors.white))));
  }
}
