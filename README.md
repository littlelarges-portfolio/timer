# Timer App

Simple timer app

<img src="https://i.imgur.com/5wBSC17.png" width="188" height="354">

Developed using the Bloc package

<img src="https://i.imgur.com/7R9nJ9e.png" width="188" height="354"><img src="https://i.imgur.com/MBOGpWu.png" width="188" height="354"><img src="https://i.imgur.com/UbbOnIF.png" width="188" height="354"><img src="https://i.imgur.com/luimGsV.png" width="188" height="354">

Demo:

<img src="demo/timer_demo.gif" width="188" height="354">